﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UPEAPP.WinForm
{
    public partial class FrmIniciarSesion : Form
    {
        public FrmIniciarSesion()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            BE.Usuario usuario = new BE.Usuario();
            BLL.Usuario negocio = new BLL.Usuario();

            usuario.UserName = txtUsername.Text;
            usuario.Password = txtPass.Text;

            if (negocio.ValidarCredencialesDeUsuario(usuario))
            {

                BE.Perfil perfil = negocio.GetPerfilByUsername(usuario.UserName);

                if (perfil.Descripcion=="ADMIN")
                {
                    FrmAdmin formularioDeAdministrador = new FrmAdmin();
                    //Oculto el iniciar sesión
                    this.Hide();

                    formularioDeAdministrador.ShowDialog();
                }
                else
                {
                    FrmVendedor formularioDelVendedor = new FrmVendedor();
                    //Oculto el iniciar sesión
                    this.Hide();

                    formularioDelVendedor.ShowDialog();

                }


            }
            else
            {
                MessageBox.Show("El usuario y clave NOOOOOOOOO son validos!!!");
            }


        }
    }
}
