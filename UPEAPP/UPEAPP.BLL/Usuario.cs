﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPEAPP.BLL
{
    public class Usuario
    {
        public bool ValidarCredencialesDeUsuario(UPEAPP.BE.Usuario usuario)
        {
            UPEAPP.DAL.Usuario dal = new UPEAPP.DAL.Usuario();

            bool retorno = false;

            foreach (BE.Usuario user in dal.Listar())
            {
                if (user.UserName == usuario.UserName && user.Password == usuario.Password)
                {
                    retorno = true;
                }
            }

            return retorno;
        }
        public BE.Perfil GetPerfilByUsername(string username)
        {
            UPEAPP.DAL.Usuario dal = new UPEAPP.DAL.Usuario();


            //Filtro a través de LINQ - Funcion anonima.
            //return dal.Listar().Where(x => x.UserName == username).FirstOrDefault().Perfil;

            BE.Perfil perfilDelUsuario = new BE.Perfil();

            foreach (BE.Usuario user in dal.Listar())
            {
                if (user.UserName == username)
                {
                    perfilDelUsuario = user.Perfil;
                }
            }

            return perfilDelUsuario;
        }
    }
}
