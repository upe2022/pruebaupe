﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPEAPP.BE
{
    public class Usuario
    {
        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }


        private string _pass;

        public string Password
        {
            get { return _pass; }
            set { _pass = value; }
        }

        private Perfil perfil;

        public Perfil Perfil
        {
            get { return perfil; }
            set { perfil = value; }
        }

    }
}
