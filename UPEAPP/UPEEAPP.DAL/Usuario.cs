﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace UPEAPP.DAL
{
    public class Usuario
    {

        public List<UPEAPP.BE.Usuario> Listar()
        {
            List<UPEAPP.BE.Usuario> retorno = new List<UPEAPP.BE.Usuario>();

            UPEAPP.BE.Usuario usuario1 = new UPEAPP.BE.Usuario();
            usuario1.UserName = "usuario1";
            usuario1.Password = "usuario1";
            BE.Perfil perfilAdmin = new BE.Perfil();

            perfilAdmin.Descripcion = "ADMIN";
            usuario1.Perfil = perfilAdmin;


            BE.Perfil perfilVendedor = new BE.Perfil();
            perfilVendedor.Descripcion = "Vendedor";

            UPEAPP.BE.Usuario usuario2 = new UPEAPP.BE.Usuario();
            usuario2.UserName = "usuario2";
            usuario2.Password = "usuario2";
            usuario2.Perfil = perfilVendedor;



            UPEAPP.BE.Usuario usuario3 = new UPEAPP.BE.Usuario();
            usuario3.UserName = "usuario3";
            usuario3.Password = "usuario3";
            usuario3.Perfil = perfilVendedor;


            retorno.Add(usuario1);
            retorno.Add(usuario2);
            retorno.Add(usuario3);

            return retorno;

        }

    }
}
